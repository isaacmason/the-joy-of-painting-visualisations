#load in necessary libraries and set theme
library(ggplot2)
theme_set(theme_minimal())

# Read in the data
d <- read.csv('most-common-connectivity.csv', as.is=TRUE)

# Only use element combinations that show up at a rate that is higher than 20 times of what the average is
d <- d[d$Count > mean(d$Count) * 20,]

# Plot graph, with additional reordering, coordinate flipping and limit setting
g <- ggplot(d, aes(x = reorder(Element, Count)))
g + geom_bar(aes(y=Count, fill=Count), width = 0.5, stat="identity") +
  theme(axis.text.x = element_text(angle=0, vjust=0.6)) + 
  coord_flip(ylim = 50:130) +
  xlab("Element") + 
  ggtitle("Most common element pairings in 'The Joy Of Painting")
