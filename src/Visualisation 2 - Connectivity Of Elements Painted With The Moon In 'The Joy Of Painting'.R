library(igraph)
library(network)

# Read in the standard dataset
data <- read.csv("elements-by-episode.csv", header=T, as.is=T)

# Read in the graph
adjancencyMatrixCSV <- read.csv("elements-by-episode-adjancency-matrix.csv", quote="")
nodeNames <- adjancencyMatrixCSV [,1]
netData <- as.matrix(adjancencyMatrixCSV [2:67])
adjMatrix <- as.matrix(netData, header=TRUE)
row.names(adjMatrix)<-nodeNames


# Create an undirected graph from the adjacency matrix with no self loops
net <- as.undirected(graph_from_adjacency_matrix(adjMatrix, weighted=TRUE, diag=FALSE))

# Remove uneccesary items
unnecessary_nodes <- c("TREES", "MOUNTAINS")
net <- delete_vertices(net, V(net)[name %in% unnecessary_nodes])

# Filtered network - min weight of 2 and min degrees of 1
net.minWeight <- delete_edges(net, which(E(net)$weight < 2))
net.significant <- delete_vertices(net.minWeight, degree(net.minWeight)==0)

node <- c("MOON")

# Neighborhood of 'STEVE_ROSS' graph - not filtered
neighborhood_origin_node_label <- node
neighborhood_origin_node <- V(net.significant)[name %in% neighborhood_origin_node_label]
neighborhood_origin_node_V <- ego(net.significant, order=1, nodes = neighborhood_origin_node, mode = "all", mindist = 0)
net.neighborhood <- induced_subgraph(net.significant, unlist(neighborhood_origin_node_V))

#Highlight lines going from STEVE_ROSS
inc.edges <- incident(net.neighborhood, V(net.neighborhood)[name == node], mode="all")

#Highlight nodes going from STEVE_ROSS
neigh.nodes <- neighbors(net.neighborhood, V(net.neighborhood)[name == node], mode="out")

#Set colors
ecol <- rep(adjustcolor(rgb(0.9, 0.9, 0.9, 0.1)), ecount(net.neighborhood))
ecol[inc.edges] <- rgb(6/255, 67/255, 134/255, 1)
esiz <- E(net)$weight*.25
vcol <- rep("white", vcount(net.neighborhood))
vcol[V(net)$name==node] <- "gold"
vcol[neigh.nodes] <- rgb(6/255, 67/255, 134/255, 1)

#Set fonts for different nodes
fonts <- rep(c(1), vcount(net.neighborhood))
fonts[neigh.nodes] <- c(2)
fonts[V(net)$name==node] <- c(4)

#Set Times font to correct font (fixes warnings)
windowsFonts(Times=windowsFont("TT Arial"))

par(bg = 'black')

windowsFonts(
  title=windowsFont("Bookman Old Style")
)

# Neighborhood of the 'STEVE_ROSS' node
plot.igraph(
  
  # Input network
  net.neighborhood,
  
  # Layout
  layout=layout_as_star(net.neighborhood, node),
  
  # Vertex styling
  vertex.size = 20,
  vertex.color = vcol,
  
  # Vertex label styling
  vertex.label.color = adjustcolor("white", .8),
  vertex.label.cex = .8,
  vertex.label.family="Times",
  vertex.label.font = fonts,
    
  # Edge styling
  weighted = TRUE,
  edge.width = esiz,
  edge.color = ecol,
  edge.curved = .2
) + 
title("Connectivity of elements painted with the moon in \n'The Joy of Painting'",
      cex.main=1.2,
      col.main="white",
      family="title")
