# Bob Ross Visualisations

This repository contains a series of visualizations analyzing Bob Ross's paintings, using the *fivethirtyeight* dataset.

The original dataset can be found here: [https://github.com/fivethirtyeight/data/tree/master/bob-ross](https://github.com/fivethirtyeight/data/tree/master/bob-ross). A copy of the dataset can be seen in the file `elements-by-episode.csv`.

Two variations of the dataset were constructed - `elements-by-episode-adjancency-matrix.csv`, is an adjacency matrix constructed from the original dataset, and `most-common-connectivity.csv` which contains the most common pairings in the data.

## Images
### Visualisation 1 - Network Structure Of Elements Painted Together In 'The Joy Of Painting'

The first is a force-directed visualisation of the network structure of elements painted together in ‘The Joy Of Painting’, using the Fruchterman Reingold network layout. The visualisation shows that there is one clear cluster or community of painting elements that are utilised in ‘The Joy Of Painting’, and suggests that external ideas are painted with the core cluster on occasions.

The vertices each represent an element present in a painting from ‘The Joy Of Painting’, and the edges communicate whether the elements have been painted together.

![Visualisation 1 - Network Structure Of Elements Painted Together In 'The Joy Of Painting'](/Figures/PNGs/Visualisation 1 - Network Structure Of Elements Painted Together In 'The Joy Of Painting'.PNG)

### Visualisation 2 - Connectivity Of Elements Painted With The Moon In 'The Joy Of Painting'

We next wanted to have a look at some more specific relationships between painting elements. The relationships between items in the core cluster didn’t produce anything meaningful, so we decided to explore some of the slightly more fringe painting elements relationships. We also wanted to limit the scope, so we explored only the direct neighbourhood of one element.

The second visualisation is another network visualisation in a star layout, showing the connectivity between elements in the direct neighbourhood of the moon painting element. The centre vertex is the moon painting element, and the blue vertices around the moon element are in the direct neighbourhood of the moon – so all of these elements have at least once been in a painting with the moon. The thickness of the edges between vertices communicates how many times the two have painted together.

![Visualisation 2 - Connectivity Of Elements Painted With The Moon In 'The Joy Of Painting'](Figures/PNGs/Visualisation 2 - Connectivity Of Elements Painted With The Moon In 'The Joy Of Painting'.PNG)

### Visualisation 3 - Most Common Element Pairings In 'The Joy Of Painting'

The third visualisation shows the most common pairings of elements in paintings.

![Visualisation 3 - Most Common Element Pairings In 'The Joy Of Painting'](/Figures/PNGs/Visualisation 3 - Most Common Element Pairings In 'The Joy Of Painting'.png)

## Requirements

The following R packages are required to generate the visualisations:

- ggplot2
- igraph
- network
- Cairo
